package com.calcina;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/teste.do")
public class TesteServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5862792396018572603L;

	@Override
	protected void service(HttpServletRequest req,
			HttpServletResponse res) throws ServletException, IOException {
		
		req.setAttribute("msg", "Msg Teste!!!");
		
		req.getRequestDispatcher("/page1.jsp").forward(req, res);
		
	}
}
